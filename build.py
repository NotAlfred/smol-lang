#!/usr/bin/env python3

import sys, subprocess

build_dir = ".buildir"
release_dir = "release"

if "clean" in sys.argv:
    subprocess.run(["rm", "-rf", build_dir])
    if "debug" in sys.argv:
        subprocess.run(["meson", "setup", build_dir, "--buildtype", "debug"])
        subprocess.run(["meson", "compile", "-C", build_dir])
        print("testing:")
        subprocess.run([f"{build_dir}/compiler/smol_compiler"])
    elif "release" in sys.argv:
        subprocess.run(["meson", "setup", build_dir, "--buildtype", "release"])
        subprocess.run(["meson", "compile", "-C", build_dir])
        subprocess.run(["mkdir", "-p", release_dir])
        subprocess.run(["cp", f"{build_dir}/compiler/smol_compiler", release_dir])

else:
    print("option doesn't exit")
    exit(-1)
    

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct binary {
    uint64_t* content;
    size_t size;
};

struct registers {
    // integer registers
    uint64_t i1;
    uint64_t i2;
    uint64_t i3;
    uint64_t i4;
    uint64_t i5;
    uint64_t i6;
    uint64_t i7;
    uint64_t i8;
    uint64_t i9;
    uint64_t iA;
    uint64_t iB;
    uint64_t iC;
    uint64_t iD;
    uint64_t iE;
    uint64_t iF;

    // floating point registers
    double d1;
    double d2;
    double d3;
    double d4;
    double d5;
    double d6;
    double d7;
    double d8;
    double d9;
    double dA;
    double dB;
    double dC;
    double dD;
    double dE;
    double dF;

    uint64_t SP; // stack pointer
    uint64_t IP; // instruction pointer

    struct {
        bool overflow;
        bool is_equal;
        bool is_greater;
        bool is_smaller;

        // this will go on later. for now this is enough
    } flags;
};


uint64_t stack[4096]    = { 0 };
struct registers status = { 0 };


static inline uint64_t
fetch_from_register(uint8_t instruction)
{
    switch (instruction) {
        case 0x01:
            // i1
            return status.i1;
        case 0x02:
            // i2
            return status.i2;
        case 0x03:
            // i3
            return status.i3;
        case 0x04:
            // i4
            return status.i4;
        case 0x05:
            // i5
            return status.i5;
        case 0x06:
            // i6
            return status.i6;
        case 0x07:
            // i7
            return status.i7;
        case 0x08:
            // i8
            return status.i8;
        case 0x09:
            // i9
            return status.i9;
        case 0x0A:
            // iA
            return status.iA;
        case 0x0B:
            // iB
            return status.iB;
        case 0x0C:
            // iC
            return status.iC;
        case 0x0D:
            // iD
            return status.iD;
        case 0x0E:
            // iE
            return status.iE;
        case 0x0F:
            // iF
            return status.iF;

        case 0x11:
            // d1
            return status.d1;
        case 0x12:
            // d2
            return status.d2;
        case 0x13:
            // d3
            return status.d3;
        case 0x14:
            // d4
            return status.d4;
        case 0x15:
            // d5
            return status.d5;
        case 0x16:
            // i6
            return status.d6;
        case 0x17:
            // i7
            return status.d7;
        case 0x18:
            // i8
            return status.d8;
        case 0x19:
            // i9
            return status.d9;
        case 0x1A:
            // iA
            return status.dA;
        case 0x1B:
            // iB
            return status.dB;
        case 0x1C:
            // iC
            return status.dC;
        case 0x1D:
            // iD
            return status.dD;
        case 0x1E:
            // iE
            return status.dE;
        case 0x1F:
            // iF
            return status.dF;

        case 0x21:
            // SP
            return status.SP;
        case 0x22:
            // IP
            return status.IP;

        default:
            fprintf(stderr, "register doesn't exist");
            exit(EXIT_FAILURE);
            break;
    }
}

static inline void
store_in_register(uint8_t r, uint64_t value)
{
    switch (r) {
        case 0x01:
            // i1
            status.i1 = (uint64_t)value;
            break;
        case 0x02:
            // i2
            status.i2 = (uint64_t)value;
            break;
        case 0x03:
            // i3
            status.i3 = (uint64_t)value;
            break;
        case 0x04:
            // i4
            status.i4 = (uint64_t)value;
            break;
        case 0x05:
            // i5
            status.i5 = (uint64_t)value;
            break;
        case 0x06:
            // i6
            status.i6 = (uint64_t)value;
            break;
        case 0x07:
            // i7
            status.i7 = (uint64_t)value;
            break;
        case 0x08:
            // i8
            status.i8 = (uint64_t)value;
            break;
        case 0x09:
            // i9
            status.i9 = (uint64_t)value;
            break;
        case 0x0A:
            // iA
            status.iA = (uint64_t)value;
            break;
        case 0x0B:
            // iB
            status.iB = (uint64_t)value;
            break;
        case 0x0C:
            // iC
            status.iC = (uint64_t)value;
            break;
        case 0x0D:
            // iD
            status.iD = (uint64_t)value;
            break;
        case 0x0E:
            // iE
            status.iE = (uint64_t)value;
            break;
        case 0x0F:
            // iF
            status.iF = (uint64_t)value;
            break;

        case 0x11:
            // d1
            status.d1 = (double)value;
            break;
        case 0x12:
            // d2
            status.d2 = (double)value;
            break;
        case 0x13:
            // d3
            status.d3 = (double)value;
            break;
        case 0x14:
            // d4
            status.d4 = (double)value;
            break;
        case 0x15:
            // d5
            status.d5 = (double)value;
        case 0x16:
            // i6
            status.d6 = (uint64_t)value;
            break;
        case 0x17:
            // i7
            status.d7 = (uint64_t)value;
            break;
        case 0x18:
            // i8
            status.d8 = (uint64_t)value;
            break;
        case 0x19:
            // i9
            status.d9 = (uint64_t)value;
            break;
        case 0x1A:
            // iA
            status.dA = (uint64_t)value;
            break;
        case 0x1B:
            // iB
            status.dB = (uint64_t)value;
            break;
        case 0x1C:
            // iC
            status.dC = (uint64_t)value;
            break;
        case 0x1D:
            // iD
            status.dD = (uint64_t)value;
            break;
        case 0x1E:
            // iE
            status.dE = (uint64_t)value;
            break;
        case 0x1F:
            // iF
            status.dF = (uint64_t)value;
            break;

        case 0x21:
            // SP
            status.SP = (uint64_t)value;
        case 0x22:
            // IP
            status.IP = (uint64_t)value;

        default:
            fprintf(stderr, "register doesn't exist");
            exit(EXIT_FAILURE);
            break;
    }
}

void
execute(struct binary file)
{
    for (size_t index = 0; index < file.size / 8; ++index) {
        uint64_t instruction = file.content[index];
        switch (instruction >> 14) {
            case 0xFF: {
                // ADD
                uint64_t r1 = fetch_from_register(
                    (instruction & 0x00FF000000000000) >> 12);
                uint64_t r2 = fetch_from_register(
                    (instruction & 0x0000FF0000000000) >> 10);

                store_in_register((instruction & 0x000000FF00000000) >> 8,
                                  r1 + r2);
                break;
            }
            case 0xFE: {
                // SUB
                uint64_t r1 = fetch_from_register(
                    (instruction & 0x00FF000000000000) >> 12);
                uint64_t r2 = fetch_from_register(
                    (instruction & 0x0000FF0000000000) >> 10);

                store_in_register((instruction & 0x000000FF00000000) >> 8,
                                  r1 - r2);
                break;
            }
            case 0XFD: {
                // LOAD
                // reead the address then store it in the correspoding register
                size_t addr_value =
                    file.content[instruction & 0x00000000FFFFFFFF];
                uint8_t r = (instruction & 0x00FF000000000000) >> 12;

                store_in_register(r, addr_value);
                break;
            }
            case 0xFC: { // STORE
                // from register to a specific place on the memory
                size_t addr = file.content[instruction & 0x00000000FFFFFFFF];
                uint64_t r =
                    fetch_from_register(instruction & 0x00FF000000000000) >> 12;

                file.content[addr] = r;
                break;
            }
            case 0xFB: {
                // AND
                uint64_t r1 = fetch_from_register(
                    (instruction & 0x00FF000000000000) >> 12);
                uint64_t r2 = fetch_from_register(
                    (instruction & 0x0000FF0000000000) >> 10);

                store_in_register((instruction & 0x000000FF00000000) >> 8,
                                  r1 & r2);
                break;
            }
            case 0xFA: {
                // ORR
                uint64_t r1 = fetch_from_register(
                    (instruction & 0x00FF000000000000) >> 12);
                uint64_t r2 = fetch_from_register(
                    (instruction & 0x0000FF0000000000) >> 10);

                store_in_register((instruction & 0x000000FF00000000) >> 8,
                                  r1 | r2);
                break;
            }
            case 0xF9: {
                // XOR
                uint64_t r1 = fetch_from_register(
                    (instruction & 0x00FF000000000000) >> 12);
                uint64_t r2 = fetch_from_register(
                    (instruction & 0x0000FF0000000000) >> 10);

                store_in_register((instruction & 0x000000FF00000000) >> 8,
                                  r1 ^ r2);
                break;
            }
            case 0xF8: {
                // JMP
                size_t addr_value =
                    file.content[instruction & 0x00000000FFFFFFFF];
                index = addr_value;
                break;
            }
            case 0xF7: {
                // JNQ
                if (!status.flags.is_equal) {
                    size_t addr_value =
                        file.content[instruction & 0x00000000FFFFFFFF];
                    index = addr_value;
                }
                break;
            }
            case 0xF6: {
                // JEQ
                if (status.flags.is_equal) {
                    size_t addr_value =
                        file.content[instruction & 0x00000000FFFFFFFF];
                    index = addr_value;
                }
                break;
            }
            case 0xF5: {
                // JLT
                if (!status.flags.is_equal && status.flags.is_smaller) {
                    size_t addr_value =
                        file.content[instruction & 0x00000000FFFFFFFF];
                    index = addr_value;
                }
                break;
            }
            case 0xF4: {
                // JGT
                if (!status.flags.is_equal && status.flags.is_greater) {
                    size_t addr_value =
                        file.content[instruction & 0x00000000FFFFFFFF];
                    index = addr_value;
                }
                break;
            }
            case 0xF3: {
                // JLE
                if (status.flags.is_equal && status.flags.is_smaller) {
                    size_t addr_value =
                        file.content[instruction & 0x00000000FFFFFFFF];
                    index = addr_value;
                }
                break;
            }
            case 0xF2: {
                // JGE
                if (status.flags.is_equal && status.flags.is_greater) {
                    size_t addr_value =
                        file.content[instruction & 0x00000000FFFFFFFF];
                    index = addr_value;
                }
                break;
            }
            case 0xF0: {
                // TODO: ITOF
                break;
            }
            case 0xEF: {
                // TODO: FTOI
                break;
            }
            case 0xEE: {
                // CMP
                uint64_t r1 = fetch_from_register(
                    (instruction & 0x00FF000000000000) >> 12);
                uint64_t r2 = fetch_from_register(
                    (instruction & 0x0000FF0000000000) >> 10);

                if (r1 == r2) {
                    status.flags.is_equal   = true;
                    status.flags.is_greater = false;
                    status.flags.is_smaller = false;
                } else if (r1 >= r2) {
                    status.flags.is_equal   = true;
                    status.flags.is_greater = true;
                    status.flags.is_smaller = false;
                } else if (r1 <= r2) {
                    status.flags.is_equal   = true;
                    status.flags.is_greater = false;
                    status.flags.is_smaller = true;
                } else {
                    status.flags.is_equal   = false;
                    status.flags.is_greater = false;
                    status.flags.is_smaller = false;
                }
                break;
            }
            default: {
                fprintf(stderr, "unknown instruction\n");
                exit(EXIT_FAILURE);
                break;
            }
        }
    }
}


struct binary
read_bin(char* bin)
{
    FILE* file = fopen(bin, "rb");

    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);
    fseek(file, 0, SEEK_SET);

    uint8_t* bin_read = (uint8_t*)calloc(size, sizeof(char));
    if (fread(bin_read, size, sizeof(char), file) >= size) {
        perror("couldn't read the binary");
        exit(EXIT_FAILURE);
    }

    struct binary binary_file = { (uint64_t*)bin_read, size };
    return binary_file;
}

int
main(int argc, char* argv[])
{
    if (argc < 2) {
        fprintf(stderr, "usage: vm [binary]");
        exit(EXIT_FAILURE);
    }

    struct binary bin = read_bin(argv[1]);
    execute(bin);

    free(bin.content);
    return 0;
}

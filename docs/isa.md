***this is a draft***

## ADD:
```0xFF(REGISTER_DEST)(REGISTER_SRC)(VALUE)```

adding two number of the same type (either float and float or int and int) together.

## SUB:
```0xFE(REGISTER_DEST)(REGISTER_SRC)(VALUE)```

subtracttion. same rules of ADD applies here too.

## LOAD:
```0xFD(REFISTER)(MEMORY)```

load from a memory to register.
	
## STORE:
```0xFC(MEMORY)(REGISTER)```

store from a register to memory.
	
## AND:
```0xFB(REGISTER_DEST)(REGISTER_SRC)(VALUE)```

doing bitwise and on a register with another register.

## ORR:
```0xFA(REGISTER_DEST)(REGISTER_SRC)(VALUE)```

bitwise or. same rules as AND applies here.

## XOR:
```0xF9(REGISTER_DEST)(REGISTER_SRC)(VALUE)```

bitwise xor. same rules as AND applies here as well.

## JMP:
```0xF8(ADDRESS)```

calling a funtion outside of the program e.g. libstd funtions.

## JNQ:
```0xF7(ADDRESS)```

calling a funtion outside of the program e.g. libstd funtions.

## JEQ:
```0xF6(ADDRESS)```

calling a funtion outside of the program e.g. libstd funtions.

## JLT:
```0xF5(ADDRESS)```

calling a funtion outside of the program e.g. libstd funtions.

## JGT:
```0xF4(ADDRESS)```

calling a funtion outside of the program e.g. libstd funtions.

## JLE:
```0xF3(ADDRESS)```

calling a funtion outside of the program e.g. libstd funtions.

## JGE:
```0xF2(ADDRESS)```

calling a funtion outside of the program e.g. libstd funtions.

## ITOF:
```0xF1(REGISTER_DEST)(REGISTER_SRC)```

integer to floating point conversions.

## FTOI:
```0xF0(REGISTER_DEST)(REGISTER_SRC)```

floating point to integer conversions.

## CMP
```0xEF(REGISTER1)(REGISTER2)```

do a compare of register1 and register2

# PUSH
```0xEE(REGISTER_SRC)```

pushing the value on top of the stack

# POP
```0xED(REGISTER_DEST)```

popping top of the stack into a register

#include "parser.hpp"


auto
Parser::parse_value() -> std::optional<Value>
{
    if (match(Token::Type::IDENT)) {
        if (match("true")) {
            return { true };
        } else if (match("false")) {
            return { false };
        } else {
            return { current_tok().data };
        }
    } else if (match(Token::Type::NUM)) {
        if (current_tok().data.find(".") == std::string::npos) {
            auto num = stoll(current_tok().data);
            return { num };
        } else {
            auto num = stold(current_tok().data);
            return { num };
        }
    } else if (match(Token::Type::STR)) {
        auto str = current_tok().data;

        return { str };
    } else {
        // TODO: ERROR HANDLING
        exit(-1);
    }
}

auto
Parser::parse_list() -> List
{
    List list {};

    skip_tok();

    while (auto x = parse_value()) {
        list.push_back(x.value());
    }

    skip_tok();

    return list;
}

auto
Parser::parse_variable() -> std::optional<Variable>
{
    set_backtrack();

    std::vector<Variable> var {};

    consume("=");

    if (auto x = parse_value()) {
        return x;
    }

    backtrack();

    return parse_list();
}

auto
Parser::parse_factor() -> std::optional<Factor>
{
    if (match(Token::Type::SLASH) || match(Token::Type::STAR)) {
        if (match(Token::Type::STAR)) {
            auto value = parse_value();
            handle_empty(value);

            skip_tok();

            return Factor { new Value { value.value() }, BinOp::MULTIPLY,
                            new Factor { parse_factor().value() } };
        } else if (match(Token::Type::SLASH)) {
            auto value = parse_value();
            handle_empty(value);

            skip_tok();

            return Factor { new Value { value.value() }, BinOp::DIVISION,
                            new Factor { parse_factor().value() } };
        }
    } else if (match(Token::Type::IDENT) || match(Token::Type::NUM)) {
        if (match(Token::Type::NUM)) {
            auto value = parse_value();
            handle_empty(value);

            skip_tok();

            return Factor { new Value { value.value() }, BinOp::NONE, NULL };
        } else if (match(Token::Type::IDENT)) {
            auto value = parse_value();
            handle_empty(value);

            skip_tok();

            return Factor { new Value { value.value() }, BinOp::NONE, NULL };
        }
    }

    return std::nullopt;
}

auto
Parser::parse_term() -> std::optional<Term>
{
    auto factor = parse_factor();
    handle_empty(factor);

    if (match(Token::Type::PLUS) || match(Token::Type::MINUS)) {
        if (match(Token::Type::STAR)) {
            auto lhs = parse_value();
            handle_empty(lhs);

            auto rhs = parse_expression();
            handle_empty(rhs);

            return Term { new Value { lhs.value() }, BinOp::MULTIPLY,
                          new Term { parse_expression().value().term } };
        } else if (match(Token::Type::SLASH)) {
            auto lhs = parse_value();
            handle_empty(lhs);

            auto rhs = parse_expression();
            handle_empty(rhs);

            return Term { new Value { lhs.value() }, BinOp::DIVISION,
                          new Term { rhs.value().term } };
        } else if (match(Token::Type::IDENT)) {
            auto value = parse_value();
            handle_empty(value);

            return Term {
                new Value {
                           value.value(),
                           },
                BinOp::NONE, NULL
            };
        } else if (match(Token::Type::NUM)) {
            auto value = parse_value();
            handle_empty(value);
            skip_tok();

            return Term {
                new Value {
                           value.value(),
                           },
                BinOp::NONE, NULL
            };
        }
    }

    return Term { .factor = factor.value().value, .bin_op = {}, .term = {} };
}

auto
Parser::parse_expression() -> std::optional<Expression>
{
    set_backtrack();

    if (match(Token::Type::LPAREN)) {
        consume(Token::Type::LPAREN);

        return parse_expression();

        consume(Token::Type::RPAREN);
    }

    if (match(Token::Type::IDENT) &&
        lookahead_tok().type == Token::Type::LPAREN) {
        auto ident = current_tok().data;
        consume(Token::Type::IDENT); // identifier

        if (not match("(")) {
            consume("(");
            backtrack();
            return Expression {};
        }

        std::vector<Expression> vars {};
        while (auto x = parse_expression()) {
            vars.push_back(x.value());
        }

        consume(")"); // )

        return Expression { .term = {}, .expression = vars, .values = {} };
    }

    if (match("true") || match("false")) {
        if (match("true")) {
            consume("true");
            return Expression { .term       = {},
                                .expression = {},
                                .values     = { true } };
        } else if (match("false")) {
            consume("false");
            return Expression { .term       = {},
                                .expression = {},
                                .values     = { false } };
        } else {
            backtrack();
        }
    }

    if (match(Token::Type::STR)) {
        auto string = current_tok().data;
        consume(Token::Type::IDENT);
        return Expression { .term       = {},
                            .expression = {},
                            .values     = { string } };
    } else {
        backtrack();
    }

    if (auto term = parse_term()) {
        if (not term.has_value()) {
            backtrack();
        } else if (match(Token::Type::PLUS)) {
            consume(Token::Type::PLUS);

            auto factor = parse_factor();
            handle_empty(factor);

            skip_tok();

            return Expression {
                .term       = { .factor = factor.value().value,
                               .bin_op = BinOp::PLUS,
                               .term   = new Term { parse_term().value() } },
                .expression = {},
                .values     = {}
            };

        } else if (match(Token::Type::MINUS)) {
            skip_tok();

            auto factor = parse_factor();
            handle_empty(factor);

            skip_tok();

            return Expression {
                .term       = { .factor = factor.value().value,
                               .bin_op = BinOp::MINUS,
                               .term   = new Term { parse_term().value() } },
                .expression = {},
                .values     = {}
            };
        } else {
            skip_tok();

            return Expression { .term       = Term { term.value() },
                                .expression = {},
                                .values     = {} };
        }
    }

    return std::nullopt;
}

auto
Parser::parse_vardef() -> std::optional<VarDef>
{
    consume("let");

    set_backtrack();

    auto identifier = next_token().data;
    consume(Token::Type::IDENT); // identifier

    consume("=>");

    Type type {};

    auto type_str = current_tok().data;
    consume(Token::Type::IDENT); // type

    if (type_str == "num") {
        type = Type::NUM;
    } else if (type_str == "fnum") {
        type = Type::FNUM;
    } else if (type_str == "bool") {
        type = Type::BOOL;
    } else if (type_str == "string") {
        type = Type::STRING;
    } else {
        backtrack();
        return std::nullopt;
    }

    return VarDef { { identifier }, type };
}

auto
Parser::parse_var() -> std::optional<Var>
{
    auto var_def = parse_vardef();
    handle_empty(var_def);

    set_backtrack();

    consume("=");

    auto expression = parse_expression();

    if (not expression.has_value()) {
        backtrack();
    }


    return Var { { var_def.value() }, { expression.value() } };
}

auto
Parser::parse_loop() -> std::optional<Loop>
{
    auto condition = parse_expression();
    handle_empty(condition);

    consume("=>");

    auto body = parse_body();

    return Loop { { condition.value() }, body };
}

auto
Parser::parse_if() -> std::optional<If>
{
    auto condition = parse_expression();
    handle_empty(condition);
    //     expected("statement condition");

    consume("=>"); // =>

    auto body = parse_body();

    return If { condition.value(), body };
}

auto
Parser::parse_statement() -> std::optional<Statement>
{
    set_backtrack();

    if (match("if")) {
        consume("if");

        auto _if = parse_if();

        if (_if.has_value()) {
            return Statement { _if };
        } else {
            backtrack();
            return std::nullopt;
        }
    }

    if (match("loop")) {
        consume("loop");

        auto loop = parse_loop();

        if (loop.has_value()) {
            return Statement { loop };
        } else {
            backtrack();
            return std::nullopt;
        }
    }

    if (match("let")) {
        consume("let");

        auto var = parse_var();

        consume(";");

        if (var.has_value()) {
            return Statement { var };
        } else {
            backtrack();
            return std::nullopt;
        }
    }

    return std::nullopt;
}

auto
Parser::parse_body() -> Body
{
    std::vector<Statement> body {};

    consume("=>");
    consume("{");

    while (auto x = parse_statement()) {
        body.push_back(x.value());
    }

    consume("}");

    return { body };
}

auto
Parser::parse_function() -> std::optional<Function>
{
    if (not match("func")) {
        return std::nullopt;
    }

    consume("func");

    auto def = parse_vardef();
    handle_empty(def);

    consume(":");
    consume("(");

    std::vector<VarDef> args {};
    while (auto x = parse_vardef()) {
        args.push_back(x.value());
        consume(","); // ,
    }

    consume(")");

    auto body = parse_body();

    return Function { def.value(), args, body };
}

auto
Parser::parse_program() -> Program
{
    std::vector<Function> functions {};

    while (auto x = parse_function()) {
        functions.push_back(x.value());
    }

    if (functions.empty()) {
        // TODO: Error handling
    }

    return { functions };
}

auto
parse(std::vector<Token> tokens) -> Program
{
    auto parser = Parser(tokens);

    return parser.parse_program();
}

auto
Parser::current_tok() -> Token
{
    return tokens.at(current);
}

auto
Parser::lookahead_tok() -> Token
{
    return tokens.at(current + 1);
}

auto
Parser::next_token() -> Token
{
    return tokens.at(current);
};

auto
Parser::skip_tok() -> void
{
    current++;
}

auto
Parser::expect(const std::string s) -> bool
{
    auto res = s == next_token().data;
    return res;
}

auto
Parser::expect(const Token::Type t) -> bool
{
    auto res = t == next_token().type;
    return res;
}

auto
Parser::consume(const std::string s) -> void
{
    // TODO: turn this into a expec() function and write another one for consume
    auto res = current_tok();
    if (s == res.data) {
        skip_tok();
    }
}

auto
Parser::consume(const Token::Type t) -> void
{
    auto res = current_tok();
    if (t == res.type) {
        skip_tok();
    }
}

auto
Parser::match(const std::string s) -> bool
{
    return s == current_tok().data;
}

auto
Parser::match(const Token::Type t) -> bool
{
    return t == current_tok().type;
}

auto
Parser::set_backtrack() -> void
{
    backtrack_point = current;
}

auto
Parser::backtrack() -> void
{
    current = backtrack_point;
}


template <typename T>
auto
Parser::handle_empty(std::optional<T> t) -> void
{
    // TODO ERROR HANDLING
    if (not t.has_value()) {
        exit(-1);
    }
}

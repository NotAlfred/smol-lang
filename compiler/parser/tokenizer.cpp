#include "parser.hpp"

#include <string>

auto
number(std::string::const_iterator it)
{
    std::string string {};

    while (std::isdigit(*it) != 0) {
        string.push_back(*it);
        it++;
    }

    return string;
}

auto
identifier(std::string::const_iterator it)
{
    std::string string {};

    while (std::isalnum(*it) != 0 || *it == '_') {
        string.push_back(*it);
        it++;
    }

    return string;
}

auto
literal(std::string::const_iterator it)
{
    std::string string {};

    while (*it != '"') {
        string.push_back(*it);
        it++;
        if (*it == '\n') {
            exit(-1);
        }
    }

    return string;
}

auto
tokenize(const std::string& source) -> std::vector<Token>
{
    std::vector<Token> tokens {};

    auto line        = 1U;
    auto pos         = 1U;
    auto source_char = source.begin();
    while (source_char != source.end()) {
        if (*source_char == ' ' || *source_char == '\t' ||
            *source_char == '\r') {
            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '\n') {
            source_char++;
            pos = 1;
            line++;

            continue;
        }

        if (*source_char == '(') {
            tokens.push_back({ "(", Token::Type::LPAREN, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == ')') {
            tokens.push_back({ ")", Token::Type::RPAREN, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '[') {
            tokens.push_back({ "[", Token::Type::LBRAC, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == ']') {
            tokens.push_back({ "]", Token::Type::RBRAC, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '{') {
            tokens.push_back({ "{", Token::Type::LBRAC, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '}') {
            tokens.push_back({ "}", Token::Type::RBRAC, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == ',') {
            tokens.push_back({ ",", Token::Type::COMMA, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '.') {
            tokens.push_back({ ".", Token::Type::PERIOD, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == ';') {
            tokens.push_back({ ";", Token::Type::SEMICOLON, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == ':') {
            tokens.push_back({ ":", Token::Type::COLON, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '$') {
            tokens.push_back({ "$", Token::Type::DOLLAR, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '+') {
            tokens.push_back({ "+", Token::Type::PLUS, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '-') {
            tokens.push_back({ "-", Token::Type::MINUS, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '/') {
            tokens.push_back({ "/", Token::Type::SLASH, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '*') {
            tokens.push_back({ "*", Token::Type::STAR, line, pos });

            pos++;
            source_char++;

            continue;
        }

        if (*source_char == '<') {
            if (*(source_char + 1) == '<') {
                tokens.push_back(
                    { "<<", Token::Type::DOUBLE_LEFT_ARROW, line, pos });

                pos         += 2;
                source_char += 2;
            } else if (*(source_char + 1) == '=') {
                tokens.push_back(
                    { "<=", Token::Type::EQUAL_LEFT_ARROW, line, pos });

                pos         += 2;
                source_char += 2;
            } else {
                tokens.push_back({ "<", Token::Type::LEFT_ARROW, line, pos });

                pos++;
                source_char++;
            }

            continue;
        }

        if (*source_char == '>') {
            if (*(source_char + 1) == '>') {
                tokens.push_back(
                    { ">>", Token::Type::DOUBLE_RIGHT_ARROW, line, pos });

                pos         += 2;
                source_char += 2;
            } else if (*(source_char + 1) == '=') {
                tokens.push_back(
                    { ">=", Token::Type::EQUAL_RIGHT_ARROW, line, pos });

                pos         += 2;
                source_char += 2;
            } else {
                tokens.push_back({ ">", Token::Type::RIGHT_ARROW, line, pos });

                pos++;
                source_char++;
            }

            continue;
        }

        if (*source_char == '=') {
            if (*(source_char + 1) == '=') {
                tokens.push_back(
                    { "==", Token::Type::DOUBLE_EQUAL, line, pos });

                pos         += 2;
                source_char += 2;
            } else if (*(source_char + 1) == '>') {
                tokens.push_back({ "=>", Token::Type::RIGHT_STICK, line, pos });

                pos         += 2;
                source_char += 2;
            } else {
                tokens.push_back({ "=", Token::Type::EQUAL, line, pos });

                pos++;
                source_char++;
            }

            continue;
        }

        if (*source_char == '"') {
            auto token_literal = literal(++source_char);
            tokens.push_back({ token_literal, Token::Type::STR, line, pos });

            pos         += token_literal.length();
            source_char += token_literal.length() + 1;

            continue;
        }

        if (std::isalpha(*source_char) != 0) {
            auto token_identifier = identifier(source_char);
            tokens.push_back({ token_identifier, Token::Type::IDENT, line, pos });

            pos         += token_identifier.length();
            source_char += token_identifier.length();

            continue;
        }

        if (std::isdigit(*source_char) != 0) {
            auto token_number = number(source_char);
            tokens.push_back({ token_number, Token::Type::NUM, line, pos });

            pos         += token_number.length();
            source_char += token_number.length();

            continue;
        }

        // in case nothing matches our criteria we add it as an unkown token
        tokens.push_back(Token { "", Token::Type::UNKOWN, line, pos });
        pos++;
        source_char++;
    }

    tokens.push_back({ "", Token::Type::END, line, pos });

    return tokens;
}

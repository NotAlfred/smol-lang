#include <optional>
#include <string>
#include <variant>
#include <vector>

struct Token {
    std::string data;

    enum Type {
        END,
        PLUS,
        MINUS,
        STAR,
        SLASH,
        LEFT_ARROW,
        DOUBLE_LEFT_ARROW,
        RIGHT_ARROW,
        DOUBLE_RIGHT_ARROW,
        EQUAL,
        DOUBLE_EQUAL,
        EQUAL_RIGHT_ARROW,
        EQUAL_LEFT_ARROW,
        RIGHT_STICK,
        RPAREN,
        LPAREN,
        RBRAC,
        LBRAC,
        RCBRAC,
        LCBRAC,
        COMMA,
        PERIOD,
        SEMICOLON,
        COLON,
        DOLLAR,
        IDENT,
        NUM,
        STR,
        UNKOWN,
    } type;

    unsigned long line;
    unsigned long pos;
};

enum Type { NUM, FNUM, STRING, BOOL };
enum BinOp { PLUS, MINUS, MULTIPLY, DIVISION, NONE };

struct Expression;
struct If;
struct Loop;
struct Var;

typedef std::variant<std::string, long long, long double, Expression> Value;
typedef std::optional<std::variant<If, Loop, Var>> Statement;
typedef std::vector<Value> List;
typedef std::variant<Value, List> Variable;
typedef Expression Condition;
typedef std::vector<Statement> Body;

struct Factor {
    Value* value;
    BinOp bin_op;
    Factor* factor;
};

struct Term {
    Value* factor;
    BinOp bin_op;
    Term* term;
};

struct VarDef {
    std::string name;
    Type type;
};

struct Expression {
    Term term;
    std::vector<Expression> expression;
    std::vector<Value> values;
};

struct Var {
    VarDef ident;
    Expression expression;
};

struct Loop {
    Condition condition;
    Body body;
};

struct If {
    Condition condition;
    Body body;
};

struct Function {
    VarDef identifier;
    std::vector<VarDef> args;
    Body body;
};

struct Program {
    std::vector<Function> functions;
};

class Parser
{
        public:
    Parser(std::vector<Token> tokens)
    {
        this->tokens = tokens;
    };

    auto parse_program() -> Program;

        private:
    auto parse_value() -> std::optional<Value>;
    auto parse_factor() -> std::optional<Factor>;
    auto parse_term() -> std::optional<Term>;
    auto parse_list() -> List;
    auto parse_variable() -> std::optional<Variable>;
    auto parse_expression() -> std::optional<Expression>;
    auto parse_statement() -> std::optional<Statement>;
    auto parse_body() -> Body;
    auto parse_var() -> std::optional<Var>;
    auto parse_vardef() -> std::optional<VarDef>;
    auto parse_loop() -> std::optional<Loop>;
    auto parse_if() -> std::optional<If>;
    auto parse_function() -> std::optional<Function>;

    std::vector<Token> tokens;
    unsigned long current         = 0U;
    unsigned long backtrack_point = 0U;


    auto current_tok() -> Token;
    auto lookahead_tok() -> Token;
    auto next_token() -> Token;
    auto skip_tok() -> void;
    auto expect(const std::string s) -> bool;
    auto expect(const Token::Type t) -> bool;
    auto consume(const std::string s) -> void;
    auto consume(const Token::Type t) -> void;
    auto match(const std::string s) -> bool;
    auto match(const Token::Type t) -> bool;
    auto set_backtrack() -> void;
    auto backtrack() -> void;

    template <typename T> auto handle_empty(std::optional<T> t) -> void;
};

auto tokenize(const std::string& source) -> std::vector<Token>;
auto parse(std::vector<Token> tokens) -> Program;

#include "codegen/codegen.hpp"
#include "opcodes.hpp"

auto
Codegen::compile(Program parselt) -> void
{
    this->binary_instructions.push_back(0xD00DD00DD00DD00D); // magic code
    this->binary_instructions.push_back(0x0000000000000001); // version

    for (auto& func : parselt.functions) {
        this->compile(func);
    }
}

auto
Codegen::compile(Function parselet) -> void
{
    // push register 1..6 to the stack to reserve them
    this->binary_instructions.push_back(PUSH | 0x01 << 12); // r1
    this->binary_instructions.push_back(PUSH | 0x02 << 12); // r2
    this->binary_instructions.push_back(PUSH | 0x03 << 12); // r3
    this->binary_instructions.push_back(PUSH | 0x04 << 12); // r4
    this->binary_instructions.push_back(PUSH | 0x05 << 12); // r5
    this->binary_instructions.push_back(PUSH | 0x06 << 12); // r6
    this->binary_instructions.push_back(PUSH | 0x21 << 12); // sp

    for (auto& arg : parselet.args) {
        this->compile(arg);
    }

    compile(parselet.body);

    // pop register 1..6 to the stack to restore them
    this->binary_instructions.push_back(PUSH | 0x01 << 12); // r1
    this->binary_instructions.push_back(PUSH | 0x02 << 12); // r2
    this->binary_instructions.push_back(PUSH | 0x03 << 12); // r3
    this->binary_instructions.push_back(PUSH | 0x04 << 12); // r4
    this->binary_instructions.push_back(PUSH | 0x05 << 12); // r5
    this->binary_instructions.push_back(PUSH | 0x06 << 12); // r6
    this->binary_instructions.push_back(PUSH | 0x21 << 12); // sp
}

auto
Codegen::compile(Body parselet) -> void
{
    for (auto& instance : parselet) {
        if (instance.has_value()) {
            this->compile(instance);
        }
    }
}

auto
Codegen::compile(If parselet) -> void
{
    // TODO: try to pre-evaluate condition. if not possible do add
    // the correct instructuions for it
}

auto
Codegen::compile(Loop parselet) -> void
{
    // TODO: same as `IF`.
}

auto
Codegen::compile(Var parselet) -> void
{
    // TODO: try to put the variable in a register and if all them were full,
    // try to push it on stack
}

auto
Codegen::compile(Expression parselet) -> void
{
    // TODO: try to evaluate `EXPRESSION` and if not possible, add the correct
    // instructions.
}

auto
Codegen::compile(VarDef parselet) -> void
{
    // TODO: put variable with its value in a register. `Vardef` can't be
    // unintialized. if it was a list, allocate memory for it.

    // NOTE: dynamiac list are not yet supperted!
}

auto
Codegen::compile(Term parselet) -> void
{
}

auto
Codegen::compile(Factor parselet) -> void
{
}

auto
Codegen::compile(Statement parselet) -> void
{
}

auto
Codegen::compile(List parselet) -> void
{
}

auto
Codegen::compile(Value parselet) -> void
{
    // TODO: determine type and use the correct type conversion if required.
}

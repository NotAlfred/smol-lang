#include "parser/parser.hpp"

#include <vector>


class Codegen
{
        public:
    Codegen(Program parslets)
    {
        this->parslets = parslets;
    }


        private:
    Program parslets;

    // final binary in a vector format. this will be written
    // to a file later.
    std::vector<unsigned long long> binary_instructions;

    // compile functions
    auto compile(Program parselet) -> void;
    auto compile(Function parselet) -> void;
    auto compile(Body parselet) -> void;
    auto compile(If parselet) -> void;
    auto compile(Loop parselet) -> void;
    auto compile(Var parselet) -> void;
    auto compile(Expression parselet) -> void;
    auto compile(VarDef parselet) -> void;
    auto compile(Term parselet) -> void;
    auto compile(Factor parselet) -> void;
    auto compile(Statement parselet) -> void;
    auto compile(List parselet) -> void;
    auto compile(Value parselet) -> void;
};

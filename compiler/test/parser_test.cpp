#include "parser/parser.hpp"

#include <cassert>
#include <iostream>

using std::is_same;

template <typename T>
auto
operator==(T t, T ast) -> bool
    requires(is_same<T, Loop>() == true || is_same<T, If>() == true)
{
    return t.body == ast.body && t.condition == ast.condition;
}

auto
operator==(Term t, Term ast)
{
    return t.factor == ast.factor && t.bin_op == ast.bin_op &&
           t.term == ast.term;
}

auto
operator==(VarDef v, VarDef ast)
{
    return v.name == ast.name && v.type == ast.type;
}

auto
operator==(Expression e, Expression ast) -> bool
{
    return e.term == e.term && e.values == ast.values &&
           e.expression == ast.expression;
}

auto
operator==(Var v, Var ast)
{
    return v.expression == ast.expression && v.ident == ast.ident;
}


auto
operator==(Body bs, Body asts) -> bool
{
    for (const auto& b : bs) {
        for (const auto& ast : asts) {
            if (b != ast) {
                return false;
            }
        }
    }

    return true;
}

auto
operator==(Function f, Function ast)
{
    return f.args == ast.args && f.body == ast.body &&
           f.identifier == ast.identifier;
}

auto
operator==(Program p, Program ast)
{
    return p.functions == ast.functions;
}

void
test_parser()
{
    auto t1   = tokenize("func main => num : () {}");
    auto p1   = Parser(t1).parse_program();
    auto ast1 = Program { { { { { "main" }, Type::NUM }, {}, {} } } };

    assert(p1 == ast1);
    std::cout << "test [1]: passed\n";


    auto t2   = tokenize("func main => num : (x => fnum) {}");
    auto p2   = Parser(t2).parse_program();
    auto ast2 = Program {
        { { { { "main" }, Type::NUM }, { { { "x" }, Type::FNUM } }, {} } }
    };

    assert(p2 == ast2);
    std::cout << "test [2]: passed\n";


    auto t3 = tokenize("func main => num : (x => fnum) { loop (true) => {} }");
    auto p3 = Parser(t3).parse_program();
    auto ast3 = Program { .functions {
        { { "main", Type::NUM },
          { { { "x" }, Type::FNUM } },
          { { Loop { { {}, {}, { true } }, {} } } } } } };

    assert(p3 == ast3);
    std::cout << "test [3]: passed\n";


    auto t4   = tokenize("func main => num : (x => fnum) {\
                            loop (true) => {\
                              let x => string; \
                              let zig => num = (4+5); \
                            } \
                          }");
    auto p4   = Parser(t4).parse_program();
    auto ast4 = Program { .functions {
        { { "main", Type::NUM },
          { { { "x" }, Type::FNUM } },
          { { Loop {
              { {}, {}, { true } },
              { Var { { { "x" }, Type::STRING }, {} },
                Var { { { "zig" }, Type::NUM },
                      Expression {
                          Term {
                              new Value { 4LL },
                              BinOp::PLUS,
                              new Term { new Value { 5LL }, BinOp::NONE, {} },
                          },
                          {},
                          {} } } } } } } } } };

    assert(p4 == ast4);
    std::cout << "test [4]: passed\n";
}

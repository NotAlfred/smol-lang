#include "parser/parser.hpp"

#include <fstream>
#include <iterator>

#ifndef NDEBUG
#include "test.hpp"
auto
test()
{
    test_parser();
}
#endif

auto
handle_input(const char* file_name)
{
    std::ifstream source(file_name);
    std::string str(std::istreambuf_iterator<char> { source }, {});

    auto tokens = tokenize(str);
    return parse(tokens);
}

int
main(int, char* argv[])
{
#ifndef NDEBUG
    test();
#endif

    auto source = handle_input(argv[1]);
    // auto bin = Codegen(source).generate();

    return 0;
}
